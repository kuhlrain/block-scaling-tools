using Localisation;
using Modding;
using Modding.Blocks;
using Modding.Mapper;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BlockScalingTools {
    public class Mod : ModEntryPoint {

        public static GameObject ModControllerObject;
        public static AudioSource ClickSound;

        public static ModKey ScaleUniformKey;
        public static ModKey ScaleAsKey;

        public static Sprite TranslateIcon;
        public static Sprite RotateIcon;
        public static Sprite ScaleIcon;
        public static Sprite CopyIcon;
        public static Sprite PasteIcon;
        public static Sprite ResetIcon;
        public static Sprite InfoIcon;

        public override void OnLoad() {
            CustomMapperTypes.AddMapperType<string, MTransform, TransformSelector>();

            // set up scale networking
            Messages.Scale = ModNetworking.CreateMessageType(new DataType[] {
                DataType.Block,
                DataType.Vector3,
                DataType.Boolean // BTM operation
            });
            ModNetworking.Callbacks[Messages.Scale] += msg => {
                Block block = (Block)msg.GetData(0);
                Vector3 newScale = (Vector3)msg.GetData(1);
                if (!(block == null || block.InternalObject == null)) {

                    if ((bool)msg.GetData(2)) {
                        TransformSelector.ScaleBlock(block.InternalObject, newScale);
                    }
                    else {
                        block.InternalObject.SetScale(newScale);
                    }

                }
            };

            // set up brace networking
            Messages.BraceWarp = ModNetworking.CreateMessageType(new DataType[] {
                DataType.Block,
                DataType.Vector3,
                DataType.Vector3
            });
            ModNetworking.Callbacks[Messages.BraceWarp] += msg => {
                BraceCode brace = ((Block)msg.GetData(0)).InternalObject as BraceCode;
                BraceWarping.WarpLocal(brace, Quaternion.Euler((Vector3)msg.GetData(1)), Quaternion.Euler((Vector3)msg.GetData(2)));
            };

            // set up surface networking
            Messages.UpdateSurface = ModNetworking.CreateMessageType(new DataType[] { DataType.Block });
            ModNetworking.Callbacks[Messages.UpdateSurface] += msg => {
                (((Block)msg.GetData(0)).InternalObject as BuildSurface)?.UpdateSurface();
            };

            // events
            Events.OnBlockInit += OnBlockInit;
            Events.OnActiveSceneChanged += (x, y) => OnSceneChanged();
            if (StatMaster.isMP) {
                OnSceneChanged();
            }

            // resources
            ModResource.OnAllResourcesLoaded += () => {
                ValueHolderDrag.CursorTexture = ModResource.GetAssetBundle("cursor").LoadAsset<Texture2D>("cursor64");
            };

            // keys
            Mod.ScaleAsKey = ModKeys.GetKey("Scale All (Uniform)");
            Mod.ScaleUniformKey = ModKeys.GetKey("Scale All (Keep)");
            TransformSelector.CopyPositionKey = ModKeys.GetKey("Copy Position");
            TransformSelector.CopyRotationKey = ModKeys.GetKey("Copy Rotation");
            TransformSelector.CopyScaleKey = ModKeys.GetKey("Copy Scale");
            TransformSelector.CopyAllKey = ModKeys.GetKey("Copy All");
            TransformSelector.PasteKey = ModKeys.GetKey("Paste");

            ScaleButton.toggleKey = ModKeys.GetKey("Scale Gizmo");
            ScaleButton.globalKey = ModKeys.GetKey("Toggle Global");
            ScaleButton.pivotKey = ModKeys.GetKey("Toggle Pivot");
            ScaleButton.linkedKey = ModKeys.GetKey("Toggle Linked");

            // surfaces
            BuildSurface.AllowThicknessChange = true;
            BuildSurface.ShowCollisionToggle = true;
            BuildSurface.ShowMassSlider = true;
            BuildSurface.ShowGlassTintSlider = true;

            CogMotorControllerHinge.ShowContactToggle = true;

            // icons
            BMWidgetPool.Pool pool = BMWidgetPool.Instance.GetPool("Prefabs/BlockMapper/LevelEditor/TransformContainer");
            GameObject mapper = pool.Get();
            pool.Add(mapper);

            TranslateIcon = UIFactory.Texture2DToSprite(mapper.transform.FindChild("Translate/Icon").GetComponent<MeshRenderer>().material.mainTexture as Texture2D);
            RotateIcon = UIFactory.Texture2DToSprite(mapper.transform.FindChild("Rotate/Icon").GetComponent<MeshRenderer>().material.mainTexture as Texture2D);
            ScaleIcon = UIFactory.Texture2DToSprite(mapper.transform.FindChild("Scale/Icon").GetComponent<MeshRenderer>().material.mainTexture as Texture2D);
            CopyIcon = UIFactory.Texture2DToSprite(mapper.transform.FindChild("Copy&Paste/CopyButton").GetComponent<MeshRenderer>().material.mainTexture as Texture2D);
            PasteIcon = UIFactory.Texture2DToSprite(mapper.transform.FindChild("Copy&Paste/PasteButton").GetComponent<MeshRenderer>().material.mainTexture as Texture2D);
            ResetIcon = UIFactory.Texture2DToSprite(mapper.transform.FindChild("Copy&Paste/ResetButton/Visual").GetComponent<MeshRenderer>().material.mainTexture as Texture2D);
            InfoIcon = GameObject.Find("HUD").transform.FindChild("TopBar/Mover/Align (Top Right Adjecent)/Buttons/INFO/Icon").GetComponent<SpriteRenderer>().sprite;

            // ModControllerObjects
            ModControllerObject = GameObject.Find("ModControllerObject");
            if (!ModControllerObject) { UnityEngine.Object.DontDestroyOnLoad(ModControllerObject = new GameObject("ModControllerObject")); }
            ModControllerObject.AddComponent<BraceWarping>();
            ModControllerObject.AddComponent<BlockScaleToolController>();
            ModControllerObject.AddComponent<MoreKeys>();
            ModControllerObject.AddComponent<UIFactory>();
            ModControllerObject.AddComponent<SurfaceSelector>();
            ModControllerObject.AddComponent<HotkeyHUDUtil>();
            if (Mods.IsModLoaded(new Guid("3c1fa3de-ec74-44e4-807c-9eced79ddd3f"))) {
                ModControllerObject.AddComponent<Mapper>();
            }

            // click sound
            GameObject clickSoundObject = new GameObject("BlockScalingToolsClickSound");
            clickSoundObject.transform.SetParent(ModControllerObject.transform);
            ClickSound = clickSoundObject.AddComponent<AudioSource>();
            ClickSound.clip = mapper.transform.FindChild("Copy&Paste").GetComponent<AudioSource>().clip;

            // easyscale uninstall warning
            if (Mods.IsModLoaded(new Guid("bb7ba333-e72b-49a8-8c0d-011a3fcadaf3"))) {
                GameObject textOriginal = GameObject.Find("HUD").transform.FindChild("TopBar/Align (Top Right)/SettingsContainer/Settings (Fold Out)/FPS/FPS").gameObject;
                var textObj = UnityEngine.Object.Instantiate(textOriginal, GameObject.Find("HUD").transform, false) as GameObject;
                textObj.transform.localPosition = new Vector3(0, -6, 0);
                var text = textObj.GetComponent<DynamicText>();
                text.SetText("UNINSTALL EASYSCALE\nBLOCK SCALING TOOLS IS NOT COMPATIBLE WITH IT\nAND YOU DON'T NEED IT");
                text.alignment = TextAlignment.Center;
                text.size = 0.3f;
                text.color = Color.white;
                UnityEngine.Object.DestroyImmediate(textObj.GetComponent<LocalisationChild>());
            }
        }

        private void OnBlockInit(Block b) {
            // add transform mapper
            b.InternalObject.AddCustom(new MTransform(b.InternalObject));

            // hide occluders
            b.GameObject.transform.FindChild("Occluder")?.gameObject.SetActive(false);
            b.GameObject.transform.FindChild("occluder")?.gameObject.SetActive(false);
            b.GameObject.transform.FindChild("placement occluders")?.gameObject.SetActive(false);

            // direction arrow
            b.GameObject?.transform.FindChild("DirectionArrow")?.gameObject.AddComponent<DirectionArrow>();

            // block specific changes
            switch (b.InternalObject.Prefab.Type) {
                case BlockType.CameraBlock: {
                    var scaler = b.GameObject.transform.FindChild("Vis").gameObject.AddComponent<ScaleRelativeToCameraMinMax>();
                    scaler.min = 0.05f;
                    scaler.max = 1f;
                    scaler.objectScale = 0.05f;
                    scaler = (b.InternalObject as FixedCameraBlock).CompoundTracker.gameObject.AddComponent<ScaleRelativeToCameraMinMax>();
                    scaler.min = 0.05f;
                    scaler.max = 1f;
                    scaler.objectScale = 0.05f;
                    break;
                }
                case BlockType.Pin: {
                    var scaler = b.GameObject.transform.FindChild("Vis").gameObject.AddComponent<ScaleRelativeToCameraMinMax>();
                    scaler.min = 0.05f;
                    scaler.max = 1f;
                    scaler.objectScale = 0.05f;
                    break;
                }
                case BlockType.RopeWinch: {
                    (b.InternalObject as SpringCode).MassSlider.DisplayInMapper = true;
                    break;
                }
                case BlockType.BuildNode: {
                    b.InternalObject.GetComponentInChildren<ScaleRelativeToCameraMinMax>().min = 0.05f;
                    break;
                }
                case BlockType.BuildEdge: {
                    ScaleRelativeToCameraMinMax distScaleLine = b.InternalObject.transform.FindChild("VisLine").gameObject.AddComponent<ScaleRelativeToCameraMinMax>();
                    distScaleLine.max = 1f;
                    distScaleLine.min = 0.1f;
                    distScaleLine.objectScale = 0.07f;

                    ScaleRelativeToCameraMinMax distScaleHandle = b.InternalObject.transform.FindChild("VisHandle").gameObject.AddComponent<ScaleRelativeToCameraMinMax>();
                    distScaleHandle.max = 1f;
                    distScaleHandle.min = 0.1f;
                    distScaleHandle.objectScale = 0.07f;
                    break;
                }
            }
        }

        public static bool SceneNotPlayable() {
            return !AdvancedBlockEditor.Instance;
        }

        private void OnSceneChanged() {
            if (Mod.SceneNotPlayable())
                return;

            // enable scaling block
            GameObject scalingBlock = GameObject.Find("HUD")?.transform.FindChild("BottomBar/Align (Bottom Left)/BLOCK BUTTONS/t_BASIC/Scaling Block (1)")?.gameObject;
            if (scalingBlock) {
                scalingBlock.SetActive(true);
                scalingBlock.transform.localPosition = new Vector3(5.275f, scalingBlock.transform.localPosition.y, scalingBlock.transform.localPosition.z);
            }

            // force enable intersections
            AllowIntersectionButton[] a = Resources.FindObjectsOfTypeAll<AllowIntersectionButton>();
            if (a.Length > 0)
                a[0].DisableIntersectionBlock();

            // setup gizmo offset display
            new GameObject("GizmoOffsetDisplay").AddComponent<GizmoOffsetDisplay>();

            // level editor tweaking
            if (SceneManager.GetActiveScene().name == "MasterSceneMultiplayer") {

                // scrollable level editor grid editing 
                LevelEditorUI ui = GameObject.Find("HUD").transform.FindChild("Snap left/LevelEditor").GetComponent<LevelEditorUI>();

                ScrollingValue scroll = ui.divisionsFields.Position.gameObject.AddComponent<ScrollingValue>();
                scroll.step = 0.1f;
                scroll.keptSteps = new float[] { 0.01f, 0.05f, 0.1f, 0.2f, 0.25f, 0.3f, 0.7f, 0.75f, 0.8f };

                scroll = ui.divisionsFields.Rotation.gameObject.AddComponent<ScrollingValue>();
                scroll.step = 5f;
                scroll.keptSteps = new float[] { 0.1f, 0.5f, 1f, 2f, 2.5f, 5f, 5.625f, 7.5f, 10f, 11.25f, 15f, 20f, 22.5f, 25f };

                scroll = ui.divisionsFields.Scale.gameObject.AddComponent<ScrollingValue>();
                scroll.step = 0.1f;
                scroll.keptSteps = new float[] { 0.005f, 0.01f, 0.05f, 0.1f, 0.2f, 0.25f, 0.3f, 0.7f, 0.75f, 0.8f };

                // force enable level editor grid editing ui
                if (!LevelEditorUI.DivisionFields.editingGrid && !StatMaster.isClient) {
                    try {
                        ui.options.ToggleDivisionsField();
                    }
                    catch { // smash
                    }
                }

                // local snap translate tools
                Transform translateTool = GameObject.Find("LevelEditorTools").transform.FindChild("EntityTransformParent/TranslateTool");
                foreach (Transform t in translateTool) {
                    EntityTranslateTool oldTool = t.GetComponent<EntityTranslateTool>();
                    if (!oldTool) {
                        continue;
                    }
                    t.gameObject.SetActive(false);

                    EntityTranslateToolLocalSnap newTool = t.gameObject.AddComponent<EntityTranslateToolLocalSnap>();
                    newTool.moveAcrossNormal = oldTool.moveAcrossNormal;
                    newTool.myRenderers = oldTool.myRenderers;
                    newTool.highlightMaterial = oldTool.highlightMaterial;
                    newTool.selectedMaterial = oldTool.selectedMaterial;
                    newTool.objToMove = translateTool.parent;
                    newTool.reverseTransforms = oldTool.reverseTransforms;

                    UnityEngine.Object.DestroyImmediate(oldTool);

                    t.gameObject.SetActive(true);
                }

                // hide gizmos while selecting
                GameObject.Find("LevelEditorTools").AddComponent<LevelEditorGizmoHide>();
            }
        }

        public static bool TransformToolDragging() {
            return StatMaster.Mode.isTranslating || StatMaster.Mode.isRotating || StatMaster.Mode.isScaling;
        }

        public static bool BlockTransformToolActive() {
            return (Machine.Active()?.CanModify ?? false)
                && !ReferenceMaster.activeMachineSimulating
                && (StatMaster.Mode.selectedTool == StatMaster.Tool.Translate
                || StatMaster.Mode.selectedTool == StatMaster.Tool.Rotate
                || StatMaster.Mode.selectedTool == StatMaster.Tool.Scale
                || StatMaster.Mode.selectedTool == StatMaster.Tool.Mirror);
        }

        public static bool EntityTransformToolActive() {
            return (Machine.Active()?.CanModify ?? false)
                && !ReferenceMaster.activeMachineSimulating
                && StatMaster.isMP
                && (StatMaster.Mode.LevelEditor.selectedTool == StatMaster.Tool.Translate
                || StatMaster.Mode.LevelEditor.selectedTool == StatMaster.Tool.Rotate
                || StatMaster.Mode.LevelEditor.selectedTool == StatMaster.Tool.Scale)
                && LevelEditor.Instance.selectionController.Selection.Count > 0;
        }

    }
}
