﻿using Modding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BlockScalingTools {

    /// <summary>
    /// Utility for creating and displaying custom hotkey hints (for AB tools).
    /// </summary>
    public class HotkeyHUDUtil : MonoBehaviour {

        public static HotkeyHUDUtil Instance { get; private set; }

        private static GameObject template;
        private static List<Tip> tips = new List<Tip>();

        private SpatialKeyHUDController helpHud;
        private GameObject tooltipParent;

        private Dictionary<GameObject, Func<bool>> tipObjects = new Dictionary<GameObject, Func<bool>>();

        class Tip {
            public string text;
            public string modifierKey;
            public string triggerKey;
            public Func<bool> isActive;

            public Tip(string text, string modifierKey, string triggerKey, Func<bool> isActive) {
                this.text = text;
                this.modifierKey = modifierKey;
                this.triggerKey = triggerKey;
                this.isActive = isActive;
            }
        }

        private void Awake() {
            Instance = this;

            Events.OnActiveSceneChanged += (x, y) => OnSceneChanged();
            if (StatMaster.isMP) {
                OnSceneChanged();
            }
        }

        private void Start() {
            // helpful tips
            AddHotkeyHint("MULTI\nSELECT", "SHIFT", null, () => !Mod.TransformToolDragging()
                && (Mod.BlockTransformToolActive() || Mod.EntityTransformToolActive()));
            AddHotkeyHint("FLIP GIZMO", "ALT", null, () => !InputManager.AdvancedBuilding.LeftShiftKey()
                && StatMaster.Mode.selectedTool != StatMaster.Tool.Mirror
                && (Mod.BlockTransformToolActive() || Mod.EntityTransformToolActive()));
            AddHotkeyHint("NO SNAP", "CTRL", null, () => Mod.TransformToolDragging());
        }

        private void OnSceneChanged() {

            tipObjects.Clear();

            if (Mod.SceneNotPlayable()) {
                return;
            }

            helpHud = GameObject.Find("HUD").transform.FindChild("CursorHUD_Hotkeys").GetComponent<SpatialKeyHUDController>();
            helpHud.transform.localScale *= 0.8f;

            tooltipParent = Instantiate(helpHud.showWithSurfaceSelection, helpHud.transform, false) as GameObject;
            tooltipParent.name = "BlockScalingToolsTips";
            tooltipParent.SetActive(true);

            Destroy(tooltipParent.transform.FindChild("Shadow").gameObject);
            Destroy(tooltipParent.transform.FindChild("Blur").gameObject);

            Destroy(tooltipParent.transform.FindChild("HotKey Entry (2)").gameObject);

            template = tooltipParent.transform.FindChild("HotKey Entry (3)").gameObject;
            template.name = "HotKey Entry";
            DestroyImmediate(template.transform.FindChild("Text").GetComponent<Localisation.LocalisationChild>());

            template.SetActive(false);

            tips.ForEach(ConstructTip);
        }

        /// <summary>
        /// Add a key hint.
        /// Either of <paramref name="modifierKey"/> or <paramref name="triggerKey"/> can be blank, but only one of them can be at once.
        /// </summary>
        /// <param name="text">The label.</param>
        /// <param name="modifierKey">The modifier key label (e.g. "SHIFT").</param>
        /// <param name="triggerKey">The trigger key label (e.g. "Q")</param>
        /// <param name="active">A function determining whether the hint should be displayed.</param>
        public static void AddHotkeyHint(string text, string modifierKey, string triggerKey, Func<bool> active) {

            if (string.IsNullOrEmpty(modifierKey) && string.IsNullOrEmpty(triggerKey)) {
                Debug.LogWarning($"Tip < {text} > has no modifier or trigger, skipping", Instance);
                return;
            }

            Tip t = new Tip(text, modifierKey, triggerKey, active);
            tips.Add(t);
            Instance?.ConstructTip(t);
        }

        private void ConstructTip(Tip t) {
            if (Mod.SceneNotPlayable()) {
                return;
            }

            GameObject key = Instantiate(template, tooltipParent.transform, false) as GameObject;
            key.SetActive(true);

            key.transform.FindChild("Text").GetComponent<DynamicText>().SetText(t.text);

            Transform modifier = key.transform.FindChild("WideKey");
            if (string.IsNullOrEmpty(t.modifierKey)) {
                modifier.gameObject.SetActive(false);
            }
            else {
                modifier.GetComponentInChildren<DynamicText>().SetText(t.modifierKey);
            }

            Transform trigger = key.transform.FindChild("Key");
            if (string.IsNullOrEmpty(t.triggerKey)) {
                modifier.localPosition = new Vector3(0.45f, 0, 0);
                trigger.gameObject.SetActive(false);
            }
            else {
                trigger.GetComponentInChildren<DynamicText>().SetText(t.triggerKey);
            }

            tipObjects.Add(key, t.isActive);
        }

        private void LateUpdate() {
            if (Mod.SceneNotPlayable()) {
                return;
            }

            int activeCount = 0;
            foreach (KeyValuePair<GameObject, Func<bool>> kvp in tipObjects) {
                bool active = kvp.Value();
                kvp.Key.SetActive(active);

                if (active) {
                    activeCount++;
                    kvp.Key.transform.localPosition = new Vector3(0, (2.2f * activeCount) - 2.2f, 0);
                }
            }

            float tipY = 0;
            if (helpHud.showWithGrid.activeSelf || helpHud.showWithNodeSelection.activeSelf) {
                tipY += 2.2f;
            }
            if (helpHud.showWithSurfaceSelection.activeSelf) {
                tipY += 4.4f;
            }
            tooltipParent.transform.localPosition = new Vector3(-7.7f, tipY, 0);
        }
    }
}
