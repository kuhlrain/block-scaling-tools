﻿using Modding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BlockScalingTools {

    /// <summary>
    /// Only allows surface nodes/edges to be selected when holding the 'surface-selector' key.
    /// </summary>
    public class SurfaceSelector : MonoBehaviour {

        private static readonly LayerMask ORIGINAL_MASK = AddPiece.Instance.layerMasky;
        private static readonly LayerMask NODE_MASK = AddPiece.CreateLayerMask(new[] { 16 });

        private static readonly ModKey FILTER_KEY = ModKeys.GetKey("Select Nodes Only");

        public static bool IsFiltering {
            get {
                return FILTER_KEY.IsDown && Mod.BlockTransformToolActive();
            }
        }

        private bool wasFiltering = false;

        void Start() {
            Events.OnSimulationToggle += _ => AllowSurfaceNodeSelectionOnly(false);
            Events.OnActiveSceneChanged += (_, __) => AllowSurfaceNodeSelectionOnly(false);
            HotkeyHUDUtil.AddHotkeyHint("SELECT\nNODES ONLY", null, "P", Mod.BlockTransformToolActive);
        }

        /// <summary>
        /// If true, replaces the selection layer mask with one that only allows surface nodes/edges to be selected.
        /// Otherwise, use the normal mask.
        /// </summary>
        /// <param name="select"></param>
        void AllowSurfaceNodeSelectionOnly(bool select) {
            AddPiece.Instance.layerMasky = select ? NODE_MASK : ORIGINAL_MASK;
            BlockSelectionToolScale.Instance?.ForceUpdateDragSelection();
        }

        void Update() {
            if (Mod.SceneNotPlayable() || ReferenceMaster.activeMachineSimulating) {
                return;
            }

            if (wasFiltering != IsFiltering) {
                wasFiltering = IsFiltering;
                AllowSurfaceNodeSelectionOnly(wasFiltering);
            }
        }
    }
}
