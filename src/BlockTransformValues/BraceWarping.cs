﻿using Modding;
using Modding.Blocks;
using System;
using System.Linq;
using UnityEngine;

namespace BlockScalingTools {

    /// <summary>
    /// Handles brace warping through the rotate tool or Object Explorer.
    /// </summary>
    public class BraceWarping : MonoBehaviour {

        void Start() {
            HotkeyHUDUtil.AddHotkeyHint("(DRAG GIZMO)\nWARP BRACE", "CTRL", "D", () => ShowHint() && !StatMaster.Mode.isRotating);
            HotkeyHUDUtil.AddHotkeyHint("WARP BRACE", "CTRL", "D", () => ShowHint() && StatMaster.Mode.isRotating);
        }

        static bool ShowHint() {
            return AdvancedBlockEditor.Instance.selectionController.Selection.Count == 1
                && AdvancedBlockEditor.Instance.selectionController.First is BraceCode;
        }

        static bool CanWarp() {
            return ShowHint()
                && StatMaster.Mode.isRotating
                && InputManager.GetControl(InputManager.Scheme.AdvancedBuilding[10], Input.GetKeyDown, true);
        }

        void Update() {
            if (!CanWarp()) {
                return;
            }

            BraceCode brace = AdvancedBlockEditor.Instance.selectionController.Selection.First() as BraceCode;

            Quaternion oldStartRotation = brace.startPoint.rotation;
            Quaternion oldEndRotation = brace.endPoint.rotation;
            Quaternion oldStartRotationLocal = brace.startPoint.localRotation;
            Quaternion oldEndRotationLocal = brace.endPoint.localRotation;

            brace.SetRotation(Machine.Active().BuildingMachine.rotation * BlockInfo.FromBlockBehaviour(brace).Rotation);

            Warp(brace, oldStartRotation, oldEndRotation, false);

            brace.ParentMachine.UndoSystem.AddAction(
                    new UndoActionBraceWarp(brace.ParentMachine, brace.Guid, brace.startPoint.localRotation, brace.endPoint.localRotation,
                    oldStartRotationLocal, oldEndRotationLocal));

            Mod.ClickSound.Play();
        }

        public static void Warp(BraceCode brace, Quaternion startRotation, Quaternion endRotation, bool local = true) {
            WarpLocal(brace, startRotation, endRotation, local);
            ModNetworking.SendToAll(
                    Messages.BraceWarp.CreateMessage(
                        new object[] { Block.From(brace), brace.startPoint.localEulerAngles, brace.endPoint.localEulerAngles }));
        }

        public static void WarpLocal(BraceCode brace, Quaternion startRotation, Quaternion endRotation, bool local = true) {
            if (local) {
                brace.startPoint.localRotation = startRotation;
                brace.endPoint.localRotation = endRotation;
            }
            else {
                brace.startPoint.rotation = startRotation;
                brace.endPoint.rotation = endRotation;
            }
            brace.SetPositionsGlobal(brace.startPoint.position,
                brace.startPoint.eulerAngles - brace.ParentMachine.boundingBoxController.transform.eulerAngles,
                brace.endPoint.position,
                brace.endPoint.eulerAngles - brace.ParentMachine.boundingBoxController.transform.eulerAngles,
                true
            );
        }
    }

}
