﻿using Modding;
using Modding.Blocks;
using Modding.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/**
 * (Almost) self contained transform mapper. Does not store its own data - instead retrieves existing data 
 * from the block.
 */
namespace BlockScalingTools {
    public class TransformSelector : CustomSelector<string, MTransform> {

        public static int transformsActive = 0;
        public static int valuesActive = 0;

        Vector3Selector position;
        Vector3Selector rotation;
        Vector3Selector scale;

        Vector3 initialPos;
        Vector3 initialRot;
        Vector3 initialScale;

        readonly List<UndoAction> undoBuffer = new List<UndoAction>();
        readonly static List<Vector3> nodeBuffer = new List<Vector3>();

        bool blockUndos = false;

        public static ModKey CopyPositionKey;
        public static ModKey CopyRotationKey;
        public static ModKey CopyScaleKey;
        public static ModKey CopyAllKey;
        public static ModKey PasteKey;

        public enum TransformType { Translate, Rotate, Scale, All, None };
        public static TransformType ClipboardContents = TransformType.None;

        static Vector3 ClipboardTranslate = Vector3.zero;
        static Vector3 ClipboardRotate = Vector3.zero;
        static Vector3 ClipboardScale = Vector3.zero;

        static bool _updateThis = false;
        static void UpdateThis() {
            _updateThis = true;
        }

        void OnEnable() {
            ReferenceMaster.onMachineModified -= x => UpdateThis();
            ReferenceMaster.onMachineModified += x => UpdateThis();
        }

        void OnDisable() {
            ReferenceMaster.onMachineModified -= x => UpdateThis();
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
            MouseOrbit.AllowWASDCamControl = true;
        }

        public static Vector3 BoundRotation(Vector3 rotation) {
            return new Vector3(Mathf.LerpAngle(0, rotation.x, 1), Mathf.LerpAngle(0, rotation.y, 1), Mathf.LerpAngle(0, rotation.z, 1));
        }

        public static Vector3 BoundScale(Vector3 scale) {
            return new Vector3(Mathf.Abs(scale.x), Mathf.Abs(scale.y), Mathf.Abs(scale.z));
        }

        Vector3 SnapComponent(Vector3 v, Vector3 initial, Vector3Selector.Component c, float snap) {
            int i = (int)c;
            if (Input.GetKey(KeyCode.LeftShift)) {
                v[i] = initial[i] + TransformTool.Snap(v[i] - initial[i], snap);
            }
            else if (Input.GetKey(KeyCode.LeftAlt)) {
                v[i] = TransformTool.Snap(v[i], snap);
            }
            return v;
        }

        Vector3 OnSetTranslate(Vector3 v, Vector3 initial, Vector3Selector.Component c) {
            return SnapComponent(v, initial, c, BlockTranslateTool.SNAP_VALUE);
        }

        Vector3 OnSetRotate(Vector3 v, Vector3 initial, Vector3Selector.Component c) {
            return BoundRotation(SnapComponent(v, initial, c, BlockRotateTool.SNAP_VALUE));
        }

        /// <summary>
        /// Transform function for scale vector. Implements uniform and proportional scaling.
        /// </summary>
        /// <param name="v">Input vector</param>
        /// <param name="initial">Input vector before changes</param>
        /// <param name="c">Active component</param>
        /// <returns>Scaled vector, or null if no scaling performed</returns>
        Vector3 OnSetScale(Vector3 v, Vector3 initial, Vector3Selector.Component c) {
            v = BoundScale(SnapComponent(v, initial, c, BlockScaleTool.SnapValue));
            int i = (int)c;
            float scale = Mathf.Abs(Mathf.Approximately(initial[i], 0) ? v[i] : (v[i] / initial[i]));
            if (Mod.ScaleUniformKey.IsDown) {
                return new Vector3(
                    c == Vector3Selector.Component.x ? v.x : initial.x * scale,
                    c == Vector3Selector.Component.y ? v.y : initial.y * scale,
                    c == Vector3Selector.Component.z ? v.z : initial.z * scale
                );
            }
            else if (Mod.ScaleAsKey.IsDown) {
                return Vector3.one * initial[i] * scale;
            }
            return v;
        }

        /// <summary>
        /// Creates mapper interface.
        /// </summary>
        protected override void CreateInterface() {
            Background.localScale = new Vector3(5, 2, 1);
            Elements.MakeBox(new Vector3(0, -0.2f, 0), new Vector2(4.5f, 1.6f), Materials.LightBackground).gameObject.name = "BG";

            initialPos = CustomMapperType.Block.Position;
            initialRot = BoundRotation(CustomMapperType.Block.Rotation.eulerAngles);
            initialScale = CustomMapperType.Block.Scale;

            // make header
            Transform header = UIFactory.Empty(Content, new Vector3(0, 0.8f, 0), "Header", layer: UIFactory.HUD_LAYER_KEYMAPPER);

            Transform infoBox = ToggleButton.Create(
                header,
                UIFactory.Sprite(
                    null,
                    Mod.InfoIcon,
                    Vector3.zero,
                    Vector2.one * 0.06f,
                    layer: UIFactory.HUD_LAYER_KEYMAPPER,
                    material: UIConfig.ICON_MAT_STENCIL
                ).transform,
                () => false,
                (x) => { return; },
                new Vector3(-1.86f, 0, 0),
                Vector3.one * 0.3f,
                layer: UIFactory.HUD_LAYER_KEYMAPPER,
                name: "Info"
            ).transform;
            Tooltip.Create(infoBox, Tooltip.Direction.Left,
                "COPY\n" +
                "[LSHIFT + A]: POSITION\n" +
                "[LSHIFT + S]: ROTATION\n" +
                "[LSHIFT + D]: SCALE\n" +
                "[LSHIFT + W]: ALL\n" +
                "\n" +
                "PASTE\n" +
                "[LSHIFT + V]\n" +
                "\n" +
                "DRAG NUMBERS LIKE SLIDERS\n" +
                "HOLD [LSHIFT]: SNAP NORMALLY\n" +
                "HOLD [LALT]: SNAP ROUNDED\n" +
                "\n" +
                "(HOLD ON SCALE ONLY)\n" +
                "[Z]: ALL - TO AXIS\n" +
                "[X]: ALL - KEEP PROPORTIONS\n",
                layer: UIFactory.HUD_LAYER_KEYMAPPER
            );

            Transform blockOrder = UIFactory.Text(header, new Vector3(-1.2f, 0), $"#{CustomMapperType.Block.transform.GetSiblingIndex()}",
                0.16f, color: Color.gray * 0.5f, layer: UIFactory.HUD_LAYER_KEYMAPPER,
                material: UIConfig.TEXT_MAT_STENCIL).transform;
            blockOrder.gameObject.AddComponent<BoxCollider>().size = Vector3.one * 0.3f;
            Tooltip.Create(blockOrder, Tooltip.Direction.Up, "BLOCK ORDER", 0.4f, UIFactory.HUD_LAYER_KEYMAPPER);

            UIFactory.Text(header, Vector3.zero, 2392, 0.195f, layer: UIFactory.HUD_LAYER_KEYMAPPER,
                material: UIConfig.TEXT_MAT_STENCIL);

            Transform copyButton = ToggleButton.Create(header,
                UIFactory.Sprite(null, Mod.CopyIcon, Vector3.zero, Vector2.one * 0.06f, layer: UIFactory.HUD_LAYER_KEYMAPPER, material: UIConfig.ICON_MAT_STENCIL).transform, () => false,
                (x) => Copy(TransformType.All), new Vector3(1.42f, 0, 0), Vector3.one * 0.3f,
                layer: UIFactory.HUD_LAYER_KEYMAPPER, name: "Copy").transform;
            Transform pasteButton = ToggleButton.Create(header,
                UIFactory.Sprite(null, Mod.PasteIcon, Vector3.zero, Vector2.one * 0.05f, layer: UIFactory.HUD_LAYER_KEYMAPPER, material: UIConfig.ICON_MAT_STENCIL).transform, () => false,
                (x) => PasteAll(), new Vector3(1.695f, 0, 0), Vector3.one * 0.3f,
                layer: UIFactory.HUD_LAYER_KEYMAPPER, name: "Paste").transform;
            Transform resetButton = ToggleButton.Create(header,
                UIFactory.Sprite(null, Mod.ResetIcon, Vector3.zero, Vector2.one * 0.07f, layer: UIFactory.HUD_LAYER_KEYMAPPER, material: UIConfig.ICON_MAT_STENCIL).transform, () => false,
                (x) => ResetAll(), new Vector3(1.97f, 0, 0), Vector3.one * 0.3f,
                layer: UIFactory.HUD_LAYER_KEYMAPPER, name: "Reset").transform;
            Tooltip.Create(copyButton, Tooltip.Direction.Up, 2319, 0.4f, layer: UIFactory.HUD_LAYER_KEYMAPPER);
            Tooltip.Create(pasteButton, Tooltip.Direction.Up, 2320, 0.4f, layer: UIFactory.HUD_LAYER_KEYMAPPER);
            Tooltip.Create(resetButton, Tooltip.Direction.Up, 2321, 0.4f, layer: UIFactory.HUD_LAYER_KEYMAPPER);

            // make vectors
            position = Vector3Selector.Create(Content, new Vector3(0, 0.275f, -1), Mod.TranslateIcon, 0.04f, initialPos, "m",
                onDragTransform: OnSetTranslate,
                copyButton: ToggleButton.Create(null, UIFactory.Sprite(null, Mod.CopyIcon, Vector3.zero, Vector2.one * 0.05f, color: new Color(1, 1, 1, 0.25f), layer: UIFactory.HUD_LAYER_KEYMAPPER, material: UIConfig.ICON_MAT_STENCIL).transform,
                () => false, x => Copy(TransformType.Translate), scale: Vector3.one * 0.15f, layer: UIFactory.HUD_LAYER_KEYMAPPER).transform);
            rotation = Vector3Selector.Create(Content, new Vector3(0, -0.225f, -1), Mod.RotateIcon, 0.08f, initialRot, "°", 15,
                onDragTransform: OnSetRotate,
                copyButton: ToggleButton.Create(null, UIFactory.Sprite(null, Mod.CopyIcon, Vector3.zero, Vector2.one * 0.05f, color: new Color(1, 1, 1, 0.25f), layer: UIFactory.HUD_LAYER_KEYMAPPER, material: UIConfig.ICON_MAT_STENCIL).transform,
                () => false, x => Copy(TransformType.Rotate), scale: Vector3.one * 0.15f, layer: UIFactory.HUD_LAYER_KEYMAPPER).transform);
            scale = Vector3Selector.Create(Content, new Vector3(0, -0.725f, -1), Mod.ScaleIcon, 0.04f, initialScale, "x",
                onDragTransform: OnSetScale,
                copyButton: ToggleButton.Create(null, UIFactory.Sprite(null, Mod.CopyIcon, Vector3.zero, Vector2.one * 0.05f, color: new Color(1, 1, 1, 0.25f), layer: UIFactory.HUD_LAYER_KEYMAPPER, material: UIConfig.ICON_MAT_STENCIL).transform,
                () => false, x => Copy(TransformType.Scale), scale: Vector3.one * 0.15f, layer: UIFactory.HUD_LAYER_KEYMAPPER).transform);

            position.OnValueChangeStart = x => UndoSystem.Enabled = false;
            rotation.OnValueChangeStart = x => UndoSystem.Enabled = false;
            scale.OnValueChangeStart = x => UndoSystem.Enabled = false;
            position.OnValueDragged = x => TransformUpdate(x, TransformType.Translate);
            rotation.OnValueDragged = x => TransformUpdate(x, TransformType.Rotate);
            scale.OnValueDragged = x => TransformUpdate(x, TransformType.Scale);
            position.OnValueChangeFinished = x => {
                TransformUpdate(x, TransformType.Translate, position.InitialValue);
                UndoSystem.Enabled = true;
            };
            rotation.OnValueChangeFinished = x => {
                TransformUpdate(x, TransformType.Rotate, rotation.InitialValue);
                UndoSystem.Enabled = true;
            };
            scale.OnValueChangeFinished = x => {
                TransformUpdate(x, TransformType.Scale, scale.InitialValue);
                UndoSystem.Enabled = true;
            };

            UpdateInterface();

            // disable position for clients because it causes the server to die
            if (StatMaster.isClient) {
                position.Lock(true);
            }
        }

        /// <summary>
        /// Update mapper interface.
        /// </summary>
        protected override void UpdateInterface() {
            if (!position || !rotation || !scale) {
                return;
            }
            position.Value = CustomMapperType.Block.Position;
            rotation.Value = BoundRotation(CustomMapperType.Block.Rotation.eulerAngles);
            scale.Value = CustomMapperType.Block.Scale;
        }

        /// <summary>
        /// Update the block transform. Adds an undo action if initial is specified.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="t">Any of (Translate, Rotate, Scale)</param>
        /// <param name="initial"></param>
        /// <exception cref="IndexOutOfRangeException">if t is not in (Translate, Rotate, Scale)</exception>
        void TransformUpdate(Vector3 value, TransformType t, Vector3? initial = null) {
            if (initial != null) {
                if (Vector3.SqrMagnitude(value - (Vector3)initial) > 1E-5) {
                    MakeUndoAction(t, value, (Vector3)initial);
                }
            }

            BlockBehaviour block = CustomMapperType.Block;
            /*if (StatMaster.Mode.Symmetry.modifying) {
                
            }*/
            switch (t) {

                case TransformType.Translate:
                    block.SetPosition(block.ParentMachine.BuildingMachine.TransformPoint(value));
                    if (block is BuildSurface) {
                        (block as BuildSurface).UpdateSurface(true);
                        ModNetworking.SendToAll(Messages.UpdateSurface.CreateMessage(new[] { Block.From(block) }));
                    }
                    break;

                case TransformType.Rotate:
                    block.SetRotation(Quaternion.Euler(value) * block.ParentMachine.BuildingMachine.rotation);

                    // surface operations
                    if (block is BuildSurface) {
                        (block as BuildSurface).UpdateSurface(true);
                        (block as BuildSurface).nodes.ToList().ForEach(x =>
                            block.ParentMachine.RotateBlock(x.Guid, Quaternion.Euler(value) * block.ParentMachine.BuildingMachine.rotation)
                        );
                        ModNetworking.SendToAll(Messages.UpdateSurface.CreateMessage(new[] { Block.From(block) }));
                    }

                    break;

                case TransformType.Scale:
                    ScaleBlock(block, value);
                    ModNetworking.SendToAll(Messages.Scale.CreateMessage(new object[] { Block.From(block), value, true }));
                    break;

                default:
                    throw new IndexOutOfRangeException($"{t} is not a valid transform type.");
            }

            if (initial != null) {
                ReferenceMaster.onMachineModified(CustomMapperType.Block.ParentMachine);
                AddPiece.Instance.UpdateMiddleOfObject();
            }
        }

        public static void ScaleBlock(BlockBehaviour block, Vector3 scale) {

            nodeBuffer.Clear();

            // dragged block operations
            GenericDraggedBlock dragged = block as GenericDraggedBlock;
            if (dragged) {
                nodeBuffer.Add(dragged.startPoint.position);
                nodeBuffer.Add(dragged.endPoint.position);
            }

            // scale operations
            Vector3 blockPos = block.transform.localPosition;
            block.SetScale(scale);
            block.transform.localPosition = blockPos;

            // dragged block operations
            if (dragged) {
                dragged.SetPositionsGlobal(nodeBuffer[0], dragged.startPoint.eulerAngles, nodeBuffer[1], dragged.endPoint.eulerAngles, true);
            }

            // surface operations
            if (block is BuildSurface) {
                (block as BuildSurface).UpdateSurface(true);
            }
        }

        // make transform vector undo action
        void MakeUndoAction(TransformType t, Vector3 newVector, Vector3 oldVector) {
            if (blockUndos) return;
            BlockBehaviour b = CustomMapperType.Block;
            switch (t) {

                case TransformType.Translate:
                    b.ParentMachine.UndoSystem.AddAction(new UndoActionMove(b.ParentMachine, b.Guid, newVector, oldVector));
                    break;

                case TransformType.Rotate:
                    b.ParentMachine.UndoSystem.AddAction(new UndoActionRotate(b.ParentMachine, b.Guid, Quaternion.Euler(newVector), Quaternion.Euler(oldVector)));
                    break;

                case TransformType.Scale:
                    b.ParentMachine.UndoSystem.AddAction(new UndoActionScale(b.ParentMachine, b.Guid, newVector, oldVector));
                    break;

                default:
                    throw new IndexOutOfRangeException($"{t} not a valid transform type.");
            }
        }

        void Update() {
            if (_updateThis) {
                _updateThis = false;
                UpdateInterface();
            }
            if (InputManager.LeftMouseButtonHeld()) return;
            MouseOrbit.AllowWASDCamControl = !InputManager.LeftHotShiftKey();
            if (CopyScaleKey.IsPressed) Copy(TransformType.Scale);
            else if (PasteKey.IsPressed) PasteAll();
            else if (CopyPositionKey.IsPressed) Copy(TransformType.Translate);
            else if (CopyRotationKey.IsPressed) Copy(TransformType.Rotate);
            else if (CopyAllKey.IsPressed) Copy(TransformType.All);
        }
        /// <summary>
        /// Copy specified vectors into clipboard.
        /// </summary>
        /// <param name="vector"></param>
        void Copy(TransformType vector) {
            if (!position || !rotation || !scale) {
                return;
            }
            if (vector == TransformType.All || vector == TransformType.Translate) {
                ClipboardTranslate = CustomMapperType.Block.Position;
                position.Flash();
            }
            if (vector == TransformType.All || vector == TransformType.Rotate) {
                ClipboardRotate = BoundRotation(CustomMapperType.Block.Rotation.eulerAngles);
                rotation.Flash();
            }
            if (vector == TransformType.All || vector == TransformType.Scale) {
                ClipboardScale = CustomMapperType.Block.Scale;
                scale.Flash();
            }
            ClipboardContents = vector;
            Mod.ClickSound.Play();
            Debug.Log($"Copied {ClipboardContents} to T{ClipboardTranslate} R{ClipboardRotate} S{ClipboardScale}", this);
        }

        /// <summary>
        /// Paste vectors from clipboard into mapper.
        /// </summary>
        void PasteAll() {
            if (!position || !rotation || !scale) {
                return;
            }
            Debug.Log($"Pasting {ClipboardContents} from T{ClipboardTranslate} R{ClipboardRotate} S{ClipboardScale}", this);
            blockUndos = true;
            BlockBehaviour b = CustomMapperType.Block;
            undoBuffer.Clear();
            if (!(b is BuildSurface) && (ClipboardContents == TransformType.All || ClipboardContents == TransformType.Translate)) {
                undoBuffer.Add(new UndoActionMove(b.ParentMachine, b.Guid, ClipboardTranslate, b.Position));
                TransformUpdate(ClipboardTranslate, TransformType.Translate);
                position.Flash();
            }
            if (ClipboardContents == TransformType.All || ClipboardContents == TransformType.Rotate) {
                undoBuffer.Add(new UndoActionRotate(b.ParentMachine, b.Guid, Quaternion.Euler(ClipboardRotate), b.Rotation));
                TransformUpdate(ClipboardRotate, TransformType.Rotate);
                rotation.Flash();
            }
            if (ClipboardContents == TransformType.All || ClipboardContents == TransformType.Scale) {
                undoBuffer.Add(new UndoActionScale(b.ParentMachine, b.Guid, ClipboardScale, b.Scale));
                TransformUpdate(ClipboardScale, TransformType.Scale);
                scale.Flash();
            }
            b.ParentMachine.UndoSystem.AddActions(undoBuffer);
            blockUndos = false;

            if (b is BuildSurface) {
                (b as BuildSurface).UpdateSurface(true);
                if (ClipboardContents == TransformType.All || ClipboardContents == TransformType.Rotate) {
                    (b as BuildSurface).nodes.ToList().ForEach(x => b.ParentMachine.RotateBlock(x.Guid, Quaternion.Euler(ClipboardRotate) * b.ParentMachine.BuildingMachine.rotation));
                }
                ModNetworking.SendToAll(Messages.UpdateSurface.CreateMessage(new object[] { Block.From(b) }));
            }

            Mod.ClickSound.Play();

            ReferenceMaster.onMachineModified(b.ParentMachine);
        }

        /// <summary>
        /// Reset block to state when mapper was opened.
        /// </summary>
        void ResetAll() {
            blockUndos = true;
            BlockBehaviour b = CustomMapperType.Block;
            undoBuffer.Clear();
            undoBuffer.Add(new UndoActionMove(b.ParentMachine, b.Guid, initialPos, b.Position));
            undoBuffer.Add(new UndoActionRotate(b.ParentMachine, b.Guid, Quaternion.Euler(initialRot), b.Rotation));
            undoBuffer.Add(new UndoActionScale(b.ParentMachine, b.Guid, initialScale, b.Scale));
            b.ParentMachine.UndoSystem.AddActions(undoBuffer);
            TransformUpdate(initialPos, TransformType.Translate);
            TransformUpdate(initialRot, TransformType.Rotate);
            TransformUpdate(initialScale, TransformType.Scale);
            blockUndos = false;
            Mod.ClickSound.Play();
            ReferenceMaster.onMachineModified(b.ParentMachine);
            Debug.Log($"Reset All", this);
        }
    }
}


