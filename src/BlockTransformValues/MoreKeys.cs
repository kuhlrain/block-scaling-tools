﻿using Modding;
using Selectors;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BlockScalingTools {

    /// <summary>
    /// Does two things: allow more than three keys to be added, and allow more than 20 chars to be used for key variables.
    /// </summary>
    class MoreKeys : MonoBehaviour {

        bool refresh = true;

        void Start() {
            Events.OnBlockInit += x => {
                x.InternalObject.MapperTypes.ForEach(y => {
                    if (y is MKey) {
                        (y as MKey).KeysChanged += () => refresh = true;
                    }
                });
            };
        }

        void Update() {
            if (!refresh || !BlockMapper.CurrentInstance) {
                return;
            }
            refresh = false;

            foreach (KeySelector k in BlockMapper.CurrentInstance.GetComponentsInChildren<KeySelector>()) {
                k.addButton.gameObject.SetActive(!k.Key.useMessage);

                Transform textBox = k.transform.parent.FindChild("TextValueHolder");
                if (textBox) {
                    textBox.GetComponent<TextHolder>().CharLimit = 99;
                }
            }
        }
    }
}
