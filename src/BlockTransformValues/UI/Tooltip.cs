﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BlockScalingTools {
    /// <summary>
    /// A simplified Besiege-like tooltip. Only supports text.
    /// </summary>
    public class Tooltip : ClickBehaviour {

        List<MeshRenderer> renderers = null;
        Transform tooltipParent;
        Vector3 extendedPosition;
        Vector3 moveDirection;

        static readonly Color TOOLTIP_TEXT_COLOR = new Color(0.075f, 0.8f, 0.7f);
        static Material TOOLTIP_MAT;

        public enum Direction {
            Up, Left, Right, Down
        }

        float fadeSpeed = 0.15f;

        bool isOpen = false;
        bool isAnimating = false;

        /// <summary>
        /// Create a tooltip with a localisation ID. Requires a collider on the parent. Requires that the tooltip be parented to an object 
        /// with 1x scale (use tooltipParent argument if parent does not satisfy this).
        /// </summary>
        /// <param name="parent">Tooltip is attached to this object. Must have a collider. Must have 1x uniform scale (use tooltipParent if not). </param>
        /// <param name="dir">Side of parent to place tooltip on.</param>
        /// <param name="localisationId">Check localisation files and/or existing objects for IDs.</param>
        /// <param name="distance">Animation distance when transitioning to visible.</param>
        /// <returns>Tooltip component</returns>
        public static Tooltip Create(Transform parent, Direction dir, int localisationId, float distance = 0.66f,
            int layer = UIFactory.HUD_LAYER) {
            DynamicText textObj = UIFactory.Text(null, new Vector3(0f, 0f, -0.1f), localisationId,
                color: TOOLTIP_TEXT_COLOR, size: 0.16f, layer: layer);
            return Create(parent, dir, textObj, distance, layer);
        }


        /// <summary>
        /// Create a tooltip with a string. Requires a collider on the parent. Requires that the tooltip be parented to an object 
        /// with 1x scale (use tooltipParent argument if parent does not satisfy this).
        /// </summary>
        /// <param name="parent">Tooltip is attached to this object. Must have a collider. Must have 1x uniform scale (use tooltipParent if not). </param>
        /// <param name="dir">Side of parent to place tooltip on.</param>
        /// <param name="text">The text to show on the tooltip.</param>
        /// <param name="distance">Animation distance when transitioning to visible.</param>
        /// <param name="tooltipParent">Parent of tooltip object. Must be uniformly scaled.</param>
        /// <returns>Tooltip component</returns>
        public static Tooltip Create(Transform parent, Direction dir, string text, float distance = 0.66f,
            int layer = UIFactory.HUD_LAYER) {
            DynamicText textObj = UIFactory.Text(null, new Vector3(0f, 0f, -0.1f), text, color: TOOLTIP_TEXT_COLOR, size: 0.16f, layer: layer);
            return Create(parent, dir, textObj, distance, layer);
        }


        /// <summary>
        /// Create a tooltip with a text object.
        /// </summary>
        /// <param name="parent">Tooltip is attached to this object. Must have a collider. Must have 1x uniform scale (use tooltipParent if not). </param>
        /// <param name="dir">Side of parent to place tooltip on.</param>
        /// <param name="textObj"></param>
        /// <param name="distance">Animation distance when transitioning to visible.</param>
        /// <returns>Tooltip component</returns>
        static Tooltip Create(Transform parent, Direction dir, DynamicText textObj, float distance = 0.66f,
            int layer = UIFactory.HUD_LAYER) {

            if (!TOOLTIP_MAT) {
                TOOLTIP_MAT = new Material(Shader.Find("Particles/Alpha Blended"));
                TOOLTIP_MAT.SetColor("_TintColor", new Color(0.053f, 0.057f, 0.066f, 1));
            }

            GameObject self = new GameObject("Tooltip");
            self.layer = layer;
            self.transform.SetParent(parent, false);
            self.transform.localScale = Vector3.one;

            textObj.transform.SetParent(self.transform, false);
            Vector3 size = textObj.GetComponent<MeshFilter>().mesh.bounds.size + new Vector3(0.4f, 0.18f, 0);
            UIFactory.Texture(self.transform, Vector3.zero, size, TOOLTIP_MAT, layer: layer);

            Vector3 offset;
            Vector3 arrowRot;
            switch (dir) {
                case Direction.Up:
                    offset = new Vector3(0, 1);
                    arrowRot = new Vector3(0, 0, 180);
                    break;
                case Direction.Down:
                    offset = new Vector3(0, -1);
                    arrowRot = Vector3.zero;
                    break;
                case Direction.Left:
                    offset = new Vector3(-1, 0);
                    arrowRot = new Vector3(0, 0, -90);
                    break;
                case Direction.Right:
                    offset = new Vector3(1, 0);
                    arrowRot = new Vector3(0, 0, 90);
                    break;
                default:
                    throw new System.ArgumentException($"Invalid direction: {dir}");
            }
            Vector3 arrowPos = new Vector3(offset.x * size.x, offset.y * size.y) * -0.5f;

            Transform arrow = UIFactory.Texture(
                self.transform,
                arrowPos + Vector3.back,
                Vector3.one * 0.17f,
                TOOLTIP_MAT,
                UIFactory.TRIANGLE_MESH,
                layer
            ).transform;
            arrow.localEulerAngles = arrowRot;

            self.transform.localPosition = offset * distance - arrowPos + Vector3.back;

            Tooltip res = parent.gameObject.AddComponent<Tooltip>();
            res.tooltipParent = self.transform;
            res.moveDirection = offset * -0.5f;
            return res;
        }


        void Start() {
            renderers = tooltipParent.GetComponentsInChildren<MeshRenderer>().ToList();
            extendedPosition = tooltipParent.localPosition;

            OnDisable();
        }

        public override void OnDisable() {
            base.OnDisable();
            StopAllCoroutines();
            isAnimating = false;
            if (renderers == null) {
                return;
            }
            foreach (MeshRenderer r in renderers) {
                Color color = r.material.color;
                r.material.color = new Color(color.r, color.g, color.b, 0);
                Color tint = r.material.GetColor("_TintColor");
                r.material.SetColor("_TintColor", new Color(tint.r, tint.g, tint.b, 0));
                r.enabled = false;
            }
        }

        public override void OnClickReleased() {
            OnMouseExit();
        }

        public override void OnCursorOver() {
            if (!UIMask.InsideMask(-1, this.transform.position)) {
                OnMouseExit();
            }
            else if (!InputManager.LeftMouseButtonHeld() && !isAnimating && !isOpen && OptionsMaster.BesiegeConfig.Tooltips) {
                StartCoroutine(Animate(true));
            }
        }

        void OnMouseExit() {
            if (isOpen) {
                isAnimating = false;
                StopAllCoroutines();
                StartCoroutine(Animate(false));
            }
        }

        /// <summary>
        /// Animates between open and closed.
        /// </summary>
        /// <param name="toActive">True if opening</param>
        /// <returns>IEnumerator for coroutine</returns>
        IEnumerator Animate(bool toActive) {

            if (isOpen == toActive) {
                yield break;
            }
            isOpen = toActive;

            isAnimating = true;

            if (toActive) {
                yield return new WaitForSeconds(0.25f);
            }

            float time = 0;
            while (time < 1) {
                time += Time.unscaledDeltaTime / fadeSpeed;
                float alpha = Mathf.Lerp(0, 1, toActive ? time : (1 - time));

                if (toActive) {
                    tooltipParent.localPosition = extendedPosition + moveDirection * (1 - alpha);
                }

                foreach (MeshRenderer r in renderers) {
                    Color color = r.material.color;
                    r.material.color = new Color(color.r, color.g, color.b, alpha);
                    Color tint = r.material.GetColor("_TintColor");
                    r.material.SetColor("_TintColor", new Color(tint.r, tint.g, tint.b, alpha * 0.5f));
                    r.enabled = alpha > 1E-6;
                }

                yield return null;
            }
            isAnimating = false;
            yield break;
        }
    }
}