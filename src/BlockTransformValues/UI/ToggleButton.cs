﻿using System;
using System.Collections;
using UnityEngine;

namespace BlockScalingTools {
    public class ToggleButton : AbstractButton {

        public Func<bool> get;
        public Action<bool> set;

        public override void OnClicked() {
            set(!get());
            UpdateMaterials(get());
        }

        public void UpdateMaterials() {
            UpdateMaterials(get());
        }

        public static ToggleButton Create(Transform parent, Transform content, Func<bool> get, Action<bool> set,
            Vector3? offset = null, Vector3? scale = null, Material activeMat = null, Material inactiveMat = null,
            int layer = UIFactory.HUD_LAYER, string name = "ToggleButton") {
            ToggleButton self = CreateObject(parent, content, offset, scale, layer, name).AddComponent<ToggleButton>();
            self.get = get;
            self.set = set;
            self.UpdateMaterials(self.get(), activeMat, inactiveMat);
            return self;
        }
    }
}