﻿using Modding;
using Selectors;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BlockScalingTools {

    /// <summary>
    /// UIFactory v2.0.5
    /// </summary>
    public class UIFactory : MonoBehaviour {

        public static Material RED_MAT { get; private set; }
        public static Material BLACK_MAT { get; private set; }
        public static Material BLACK_MAT_MASKED { get; private set; }
        public static Material WHITE_MAT { get; private set; }
        public static Material BLUR_MAT { get; private set; }
        public static Material GRAY_MAT { get; private set; }
        public static Material CLEAR_MAT { get; private set; }

        public const int HUD_LAYER = 13;
        public const int HUD_LAYER_KEYMAPPER = 19;

        public static Mesh SQUARE_MESH { get; private set; }
        public static Mesh TRIANGLE_MESH { get; private set; }

        static GameObject textOriginal;
        static GameObject inputFieldOriginal;

        void Awake() {
            RED_MAT = new Material(Shader.Find("Particles/Alpha Blended"));
            BLACK_MAT = new Material(Shader.Find("Particles/Alpha Blended"));
            BLACK_MAT_MASKED = new Material(Shader.Find("Masked/Alpha Blended"));
            WHITE_MAT = new Material(Shader.Find("Particles/Alpha Blended"));
            BLUR_MAT = new Material(Shader.Find("Custom/TooltipBlur (Larger)"));
            GRAY_MAT = new Material(Shader.Find("Particles/Alpha Blended"));
            CLEAR_MAT = new Material(Shader.Find("Particles/Alpha Blended"));

            RED_MAT.SetColor("_TintColor", new Color(0.459f, 0.067f, 0.145f));
            CLEAR_MAT.SetColor("_TintColor", new Color(0, 0, 0, 0));

            BLACK_MAT.SetColor("_TintColor", new Color(0.015f, 0.015f, 0.022f, 0.1f));
            BLACK_MAT_MASKED.SetColor("_TintColor", new Color(0.015f, 0.015f, 0.022f, 0.2f));
            WHITE_MAT.SetColor("_TintColor", Color.white);
            BLUR_MAT.SetColor("_TintColor", Color.white);
            GRAY_MAT.SetColor("_TintColor", Color.gray);

            SQUARE_MESH = new Mesh() {
                vertices = new Vector3[4] {
                    new Vector3(-0.5f, -0.5f, 0),
                    new Vector3(0.5f, -0.5f, 0),
                    new Vector3(-0.5f, 0.5f, 0),
                    new Vector3(0.5f, 0.5f, 0)
                },
                triangles = new int[6] {
                    // lower left triangle
                    0, 2, 1,
                    // upper right triangle
                    2, 3, 1
                },
                normals = new Vector3[4] {
                -Vector3.forward,
                -Vector3.forward,
                -Vector3.forward,
                -Vector3.forward
            },
                uv = new Vector2[4] {
                new Vector2(0, 0),
                new Vector2(1, 0),
                new Vector2(0, 1),
                new Vector2(1, 1)
            },
                name = "SquareMesh"
            };

            TRIANGLE_MESH = new Mesh() {
                vertices = new Vector3[] {
                    new Vector3(-0.5f, 0, 0),
                    new Vector3(0.5f, 0, 0),
                    new Vector3(0, 1, 0)
                },
                triangles = new int[3] {
                    0, 1, 2
                },
                normals = new Vector3[] {
                -Vector3.forward,
                -Vector3.forward,
                -Vector3.forward,
            },
                uv = new Vector2[] {
                new Vector2(0, 0),
                new Vector2(1, 0),
                new Vector2(0, 1),
            },
                name = "TriMesh"
            };

            Events.OnActiveSceneChanged += OnSceneChanged;
            if (StatMaster.isMP) {
                OnSceneChanged(SceneManager.GetActiveScene(), SceneManager.GetActiveScene());
            }
        }

        private void OnSceneChanged(Scene arg0, Scene arg1) {
            if (Mod.SceneNotPlayable())
                return;

            // setup text
            textOriginal = GameObject.Find("HUD").transform.FindChild("TopBar/Align (Top Right)/SettingsContainer/Settings (Fold Out)/FPS/FPS").gameObject;

            if (!UIConfig.TEXT_MAT_STENCIL) {
                UIConfig.TEXT_MAT_STENCIL = new Material(textOriginal.GetComponent<MeshRenderer>().sharedMaterial);
                UIConfig.TEXT_MAT_STENCIL.shader = Shader.Find("GUI/3D Text Shader [Stencil]");
                UIConfig.TEXT_MAT_STENCIL.SetFloat("_StencilVal", 1);
            }

            // setup input field
            inputFieldOriginal = Mod.ModControllerObject.transform.FindChild("UIFactory InputField")?.gameObject;
            if (!inputFieldOriginal) {
                BMWidgetPool.Pool pool = BMWidgetPool.Instance.GetPool("Prefabs/BlockMapper/LevelEditor/EventContainer");
                GameObject mapper = pool.Get();
                pool.Add(mapper);

                inputFieldOriginal = Instantiate(mapper.transform.FindChild("Container/Variable/KeyHolder").gameObject,
                    Mod.ModControllerObject.transform, false) as GameObject;
                inputFieldOriginal.name = "UIFactory InputField";

                DestroyImmediate(inputFieldOriginal.transform.FindChild("Var").gameObject);
                inputFieldOriginal.transform.FindChild("Background").gameObject.SetActive(false);
                inputFieldOriginal.transform.FindChild("InputFlash").GetComponent<MeshRenderer>().sharedMaterial = UIConfig.CURSOR_MAT;
                GameObject textVisObject = inputFieldOriginal.transform.FindChild("r_Text").gameObject;
                textVisObject.GetComponent<MeshRenderer>().sharedMaterial = textOriginal.GetComponent<MeshRenderer>().sharedMaterial;
                DestroyImmediate(textVisObject.GetComponent<Localisation.LocalisationChild>());
                DynamicText textVis = textVisObject.GetComponent<DynamicText>();
                textVis.SetText(textVis.serializedText = "");
                inputFieldOriginal.SetActive(false);
                foreach (Transform t in inputFieldOriginal.transform) {
                    t.gameObject.layer = UIFactory.HUD_LAYER;
                }
            }
        }

        public static Transform Empty(Transform parent, Vector3 offset, string name, int layer = UIFactory.HUD_LAYER) {
            Transform res = new GameObject(name).transform;
            res.SetParent(parent, false);
            res.localPosition = offset;
            res.gameObject.layer = layer;
            return res;
        }

        /// <summary>
        /// Creates a UI background (with a center pivot, blur, and correct color)
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="offset"></param>
        /// <param name="scale"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static GameObject Background(Transform parent, Vector3 offset, Vector3 scale, bool blur = false, string name = "BG") {
            GameObject res = Texture(parent, offset, scale, blur ? UIConfig.BACKGROUND_MASK_MAT : UIConfig.BACKGROUND_MAT, name: name);
            if (blur) {
                List<Renderer> blurRenderers = UIBlurManager.Instance.blurRenderers.ToList();
                blurRenderers.Add(Texture(res.transform, Vector3.zero, Vector3.one, UIFactory.BLUR_MAT, name: "Blur").GetComponent<Renderer>());
                UIBlurManager.Instance.blurRenderers = blurRenderers.ToArray();
            }
            return res;
        }

        /// <summary>
        /// Creates a UI mesh texture object. Defaults to a square mesh with center pivot.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="offset"></param>
        /// <param name="scale"></param>
        /// <param name="sharedMaterial"></param>
        /// <param name="name"></param>
        /// <param name="mesh"></param>
        /// <returns></returns>
        public static GameObject Texture(Transform parent, Vector3 offset, Vector3 scale, Material sharedMaterial,
            Mesh mesh = null, int layer = UIFactory.HUD_LAYER, string name = "Texture") {

            GameObject obj = new GameObject(name);
            obj.layer = layer;
            obj.transform.SetParent(parent, false);
            obj.transform.localPosition = offset;
            obj.transform.localScale = scale;

            MeshFilter filter = obj.AddComponent<MeshFilter>();
            filter.mesh = mesh ?? SQUARE_MESH;

            obj.AddComponent<MeshRenderer>().sharedMaterial = sharedMaterial;

            return obj;
        }

        public static Sprite Texture2DToSprite(Texture2D icon) {
            return UnityEngine.Sprite.Create(icon,
                new Rect(0, 0, icon.width, icon.height),
                new Vector2(0.5f, 0.5f),
                12.5f);
        }

        /// <summary>
        /// Creates a UI Sprite (with auto color and center pivot). Default offset is 0. Default scale is 0.08x. Default color is white.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="sprite"></param>
        /// <param name="offset"></param>
        /// <param name="scale"></param>
        /// <param name="color"></param>
        /// <param name="layer"></param>
        /// <param name="material"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static SpriteRenderer Sprite(Transform parent, Sprite sprite, Vector3? offset = null, Vector3? scale = null, Color? color = null,
            int layer = UIFactory.HUD_LAYER, Material material = null, string name = "Sprite") {
            GameObject obj = new GameObject(name);
            obj.layer = layer;
            obj.transform.SetParent(parent, false);
            obj.transform.localPosition = offset ?? Vector3.zero;
            obj.transform.localScale = scale ?? (Vector3.one * 0.08f);

            SpriteRenderer renderer = obj.AddComponent<SpriteRenderer>();
            renderer.sprite = sprite;
            renderer.color = color ?? Color.white;
            renderer.sharedMaterial = material ?? UIFactory.WHITE_MAT;

            return renderer;
        }

        /// <summary>
        /// Create an input box (with localisation ID for placeholder text)
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="offset"></param>
        /// <param name="set">Action(string)</param>
        /// <param name="placeholderLocId"></param>
        /// <param name="size"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static TextHolder InputField(Transform parent, Vector3 offset, Selectors.TextChangeHandler set, int placeholderLocId,
            float size = 0.195f, int layer = UIFactory.HUD_LAYER, string name = "InputField") {
            DynamicText placeholder = UIFactory.Text(null, Vector3.zero, placeholderLocId, 0.195f, new Color(1, 1, 1, 0.15f), layer,
                TextAlignment.Center, DynamicTextAnchor.BaselineLeft, name: "Placeholder");
            return InputField(parent, offset, set, placeholder, size, name);
        }

        /// <summary>
        /// Create an input box (with explicit placeholder text)
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="offset"></param>
        /// <param name="set">Action(string)</param>
        /// <param name="placeholderText"></param>
        /// <param name="size"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static TextHolder InputField(Transform parent, Vector3 offset, Selectors.TextChangeHandler set, string placeholderText,
            float size = 0.195f, int layer = UIFactory.HUD_LAYER, string name = "InputField") {
            DynamicText placeholder = UIFactory.Text(null, Vector3.zero, placeholderText, 0.195f, new Color(1, 1, 1, 0.15f), layer,
                TextAlignment.Center, DynamicTextAnchor.BaselineLeft, name: "Placeholder");
            return InputField(parent, offset, set, placeholder, size, name);
        }

        private static TextHolder InputField(Transform parent, Vector3 offset, Selectors.TextChangeHandler set, DynamicText placeholder,
            float size = 0.195f, string name = "InputField") {
            GameObject self = Instantiate(inputFieldOriginal, parent, false) as GameObject;
            self.name = name;
            self.transform.localPosition = offset;
            self.transform.localScale = (size / 0.195f) * Vector3.one;
            placeholder.fontStyle = FontStyle.Italic;
            placeholder.transform.SetParent(self.transform, false);
            placeholder.transform.localPosition = self.transform.FindChild("r_Text").localPosition;
            self.AddComponent<InputFieldPlaceholder>().placeholder = placeholder.gameObject;
            TextHolder res = self.GetComponent<TextHolder>();
            res.invokeEventOnEveryChange = true;
            res.TextChanged += set;
            self.SetActive(true);
            return res;
        }

        /// <summary>
        /// Creates a DynamicText object with a LocalisationID.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="offset"></param>
        /// <param name="localisationId"></param>
        /// <param name="size"></param>
        /// <param name="color"></param>
        /// <param name="alignment"></param>
        /// <param name="anchor"></param>
        /// <param name="material">Must be an existing font material.</param>
        /// <param name="maxCharWidth"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static DynamicText Text(Transform parent, Vector3 offset,
            int localisationId, float size = 0.192f, Color? color = null, int layer = UIFactory.HUD_LAYER,
            TextAlignment alignment = TextAlignment.Center, DynamicTextAnchor anchor = DynamicTextAnchor.MiddleCenter,
            int? maxCharWidth = null, Material material = null, string name = "Text") {

            DynamicText label = Text(parent, offset, size, color, layer, alignment, anchor, maxCharWidth, material, name);
            label.gameObject.layer = 19;

            label.gameObject.SetActive(false);
            Localisation.LocalisationChild loc = label.gameObject.AddComponent<Localisation.LocalisationChild>();
            loc.translationID = localisationId;
            label.gameObject.SetActive(true);

            return label;
        }

        /// <summary>
        /// Creates a DynamicText object with a specified string.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="offset"></param>
        /// <param name="text"></param>
        /// <param name="size"></param>
        /// <param name="color"></param>
        /// <param name="alignment"></param>
        /// <param name="anchor"></param>
        /// <param name="material">Must be an existing font material.</param>
        /// <param name="maxCharWidth"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static DynamicText Text(Transform parent, Vector3 offset,
            string text, float size = 0.192f, Color? color = null, int layer = UIFactory.HUD_LAYER,
            TextAlignment alignment = TextAlignment.Center, DynamicTextAnchor anchor = DynamicTextAnchor.MiddleCenter,
            int? maxCharWidth = null, Material material = null, string name = "Text") {

            DynamicText label = Text(parent, offset, size, color, layer, alignment, anchor, maxCharWidth, material, name);

            DestroyImmediate(label.GetComponent<Localisation.LocalisationChild>());
            label.SetText(label.serializedText = text);

            return label;
        }

        private static DynamicText Text(Transform parent, Vector3 offset,
            float size = 0.192f, Color? color = null, int layer = UIFactory.HUD_LAYER, TextAlignment alignment = TextAlignment.Right,
            DynamicTextAnchor anchor = DynamicTextAnchor.MiddleCenter, int? maxCharWidth = null, Material material = null, string name = "Text") {

            DynamicText label = ((GameObject)Instantiate(textOriginal, parent, false)).GetComponent<DynamicText>();
            label.transform.localPosition = offset;
            label.transform.localScale = Vector3.one;
            label.gameObject.layer = layer;
            label.size = size;
            label.alignment = alignment;
            label.anchor = anchor;
            label.color = color ?? Color.white;
            label.gameObject.name = name;
            label.letterSpacing = 0;

            if (maxCharWidth != null) {
                label.gameObject.AddComponent<TextScaler>().maxChars = (int)maxCharWidth;
            }
            if (material != null) {
                label.autoSetFontMaterial = false;
                label.GetComponent<Renderer>().sharedMaterial = material;
            }

            return label;
        }
    }
}