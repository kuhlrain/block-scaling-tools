﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Modding;

namespace BlockScalingTools {
    class LevelEditorGizmoHide : MonoBehaviour {

        LevelEditor levelEditor;

        void Start() {
            levelEditor = LevelEditor.Instance;
        }

        void LateUpdate() {
            if (levelEditor.ToolTransform.gameObject.activeSelf && (InputManager.LevelEditor.LeftShiftKey() || levelEditor.selectionController.IsDragging)) {
                ShowToolGizmo(StatMaster.Tool.None);
            }
            else if (!InputManager.LevelEditor.LeftShiftKey() && !levelEditor.selectionController.IsDragging) {
                ShowToolGizmo(levelEditor.CurrentState);
            }
        }

        void ShowToolGizmo(StatMaster.Tool tool) {
            if (tool != StatMaster.Tool.None) {
                if (!levelEditor.ToolTransform.gameObject.activeSelf) {
                    levelEditor.ToolTransform.gameObject.SetActive(true);
                }
                for (int i = 0; i < levelEditor.Tools.Length; i++) {
                    levelEditor.Tools[i].gameObject.SetActive(levelEditor.selectionController.Count > 0 && i == (int)tool);
                }
            }
            else if (levelEditor.ToolTransform.gameObject.activeSelf) {
                levelEditor.ToolTransform.gameObject.SetActive(false);
            }
        }
    }
}
