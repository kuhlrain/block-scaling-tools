﻿using Modding;
using Selectors;
using System;
using System.Collections;
using System.Linq;
using UnityEngine;

namespace BlockScalingTools {
    public class ScaleButton : MonoBehaviour {

        public static ScaleButton Instance;
        public static ModKey toggleKey;

        public static ModKey globalKey;
        public static ModKey pivotKey;
        public static ModKey linkedKey;

        public Renderer bgRend;
        public ValueHolder snappingValue;

        public UIButtonExtended globalButton;
        public UIButtonExtended pivotButton;
        public UIButtonExtended linkedButton;

        public MachineToolController toolControllerCode;

        private void Start() {
            Instance = this;

            globalButton.Down += ToggleGlobal;
            pivotButton.Down += TogglePivot;
            linkedButton.Down += ToggleLinked;
            snappingValue.SetValue(BlockScaleTool.SnapValue);
            snappingValue.ValueChanged += SetSnap;

            bgRend = transform.FindChild("BG").GetComponent<Renderer>();

            ToggleAdvanced(StatMaster.advancedBuilding);
            BlockScaleToolController.Instance.ToggleAdvanced(StatMaster.advancedBuilding);
            transform.FindChild("AdvancedTooltip/TooltipText").GetComponent<TextMesh>().text = "SCALE BLOCKS";

            ReferenceMaster.ToolDisable += OffExternal;
            ReferenceMaster.onAdvancedBuildingToggled += () => Instance.ToggleAdvanced(StatMaster.advancedBuilding);
            StatMaster.Mode.ToolChanged += SetGizmo;

            AdvancedBlockEditor.ScaleToolSet += ScaleOn;
        }

        void SetGizmo(StatMaster.Tool t) {
            if (t == StatMaster.Tool.Scale) {
                AdvancedBlockEditor.Instance.Tools.ToList().ForEach(x => x.gameObject.SetActive(false));
                BlockScaleToolController.HandleInstance.SetActive(true);
            }
            else {
                BlockScaleToolController.HandleInstance.SetActive(false);
            }
            AdvancedBlockEditor.Instance.UpdateGizmo();
        }

        void OnDestroy() {
            ReferenceMaster.ToolDisable -= OffExternal;
            ReferenceMaster.onAdvancedBuildingToggled -= () => Instance.ToggleAdvanced(StatMaster.advancedBuilding);
            StatMaster.Mode.ToolChanged -= SetGizmo;
            AdvancedBlockEditor.ScaleToolSet -= ScaleOn;
        }

        void Update() {
            if (!(bgRend.gameObject.activeSelf && bgRend.gameObject.activeInHierarchy) && StatMaster.Mode.selectedTool == StatMaster.Tool.Scale) {
                ScaleOn();
            }
            else if (bgRend.gameObject.activeSelf && bgRend.gameObject.activeInHierarchy && StatMaster.Mode.selectedTool != StatMaster.Tool.Scale) {
                OffExternal();
            }

            if (toggleKey.IsPressed) { OnMouseDown(); }
            if (globalKey.IsPressed) { ToggleGlobal(); }
            if (pivotKey.IsPressed) { TogglePivot(); }
            if (linkedKey.IsPressed) { ToggleLinked(); }
        }

        public void ToggleAdvanced(bool isOn) {
            if (isOn) {
                Toggled();
            }
            else {
                OffExternal();
            }
        }

        private void Toggled() {
            globalButton.BG.SetActive(StatMaster.Mode.Transform.global);
            pivotButton.BG.SetActive(StatMaster.Mode.Transform.pivot);
            linkedButton.BG.SetActive(StatMaster.Mode.Transform.linked);
        }

        public void SetSnap(float val) {
            BlockScaleTool.SnapValue = val;
        }

        public void ToggleGlobal() {
            StatMaster.Mode.Transform.global = !StatMaster.Mode.Transform.global;
            AdvancedBlockEditor.ChangedGlobalToggle(StatMaster.Mode.Transform.global);
            if (StatMaster.Mode.Transform.global) {
                globalButton.ToggleBG(true);
                TranslateButton.Instance.globalButton.ToggleBG(true);
                MachineRotation.Instance.globalButton.ToggleBG(true);
                MirrorButton.Instance.globalButton.ToggleBG(true);
            }
            else {
                globalButton.ToggleBG(false);
                TranslateButton.Instance.globalButton.ToggleBG(false);
                MachineRotation.Instance.globalButton.ToggleBG(false);
                MirrorButton.Instance.globalButton.ToggleBG(false);
            }
        }

        public void TogglePivot() {
            StatMaster.Mode.Transform.pivot = !StatMaster.Mode.Transform.pivot;
            AdvancedBlockEditor.ChangedPivotToggle(StatMaster.Mode.Transform.pivot);
            if (StatMaster.Mode.Transform.pivot) {
                pivotButton.ToggleBG(true);
                TranslateButton.Instance.pivotButton.ToggleBG(true);
                MachineRotation.Instance.pivotButton.ToggleBG(true);
                MirrorButton.Instance.pivotButton.ToggleBG(true);
            }
            else {
                pivotButton.ToggleBG(false);
                TranslateButton.Instance.pivotButton.ToggleBG(false);
                MachineRotation.Instance.pivotButton.ToggleBG(false);
                MirrorButton.Instance.pivotButton.ToggleBG(false);
            }
        }

        public void ToggleLinked() {
            StatMaster.Mode.Transform.linked = !StatMaster.Mode.Transform.linked;
            AdvancedBlockEditor.ChangedLinkToggle(StatMaster.Mode.Transform.linked);
            if (StatMaster.Mode.Transform.linked) {
                linkedButton.ToggleBG(true);
                TranslateButton.Instance.linkedButton.ToggleBG(true);
                MachineRotation.Instance.linkedButton.ToggleBG(true);
            }
            else {
                linkedButton.ToggleBG(false);
                TranslateButton.Instance.linkedButton.ToggleBG(false);
                MachineRotation.Instance.linkedButton.ToggleBG(false);
            }
        }

        private void OnMouseDown() {
            if (!enabled) {
                bgRend.gameObject.SetActive(false);
                return;
            }
            if (!Machine.Active() || Machine.Active().isSimulating || !Machine.Active().CanModify) {
                return;
            }
            if (StatMaster.Mode.selectedTool == StatMaster.Tool.Scale) {
                ScaleOff();
            }
            else {
                ScaleOn();
                Toggled();
                ReferenceMaster.ResetLevelEditor();
            }
        }

        public void ScaleOn() {
            bgRend.gameObject.SetActive(true);
            StatMaster.Mode.selectedTool = StatMaster.Tool.Scale;
            if (!ReferenceMaster.ToolsEnabled.Contains(this)) {
                ReferenceMaster.ToolsEnabled.Add(this);
            }
            toolControllerCode.translateToolCode?.OffExternal();
            toolControllerCode.keyMapButtonCode?.OffExternal();
            toolControllerCode.eraseButtonCode?.OffExternal();
            toolControllerCode.symmetryPivotButtonCode?.OffExternal();
            toolControllerCode.rotateToolCode?.OffExternal();
            toolControllerCode.mirrorToolCode?.OffExternal();
            toolControllerCode.paintToolCode?.OffExternal();
        }

        public void ScaleOff() {
            OffExternal();
            MachineToolController.Instance.DisableAll();
        }

        public void OffExternal() {
            bgRend?.gameObject.SetActive(false);
            if (ReferenceMaster.ToolsEnabled.Contains(this)) {
                ReferenceMaster.ToolsEnabled.Remove(this);
            }
        }

    }
}